import {ProductItemComponent} from './product-item/product-item.component';

export const containers: any[] = [ProductItemComponent, ProductItemComponent];
export * from './product-item/product-item.component';
export * from './products/products.component';
