import {ProductItemComponent, ProductsComponent} from './controller';
import {PizzaDisplayComponent, PizzaFormComponent, PizzaItemComponent, PizzaToppingComponent} from './view';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import * as fromController from './controller';
import {ReactiveFormsModule} from '@angular/forms';
export const ROUTES: Routes = [
  {
    path: '',
    component: fromController.ProductsComponent
  },
  { path: ':id',
    component: fromController.ProductItemComponent
  },
  {
    path: 'new',
    component: fromController.ProductItemComponent
  }
]
@NgModule({
  declarations: [PizzaItemComponent, PizzaDisplayComponent,
    PizzaFormComponent, PizzaToppingComponent, ProductItemComponent, ProductsComponent],
  exports: [ PizzaItemComponent, PizzaDisplayComponent,
    PizzaFormComponent, PizzaToppingComponent, ProductItemComponent, ProductsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES)
  ]
})
export class ProductsModule {}
