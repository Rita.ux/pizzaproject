import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Pizza} from '../../models/pizza.model';
import {Topping} from '../../models/topping.model';
import {FormBuilder, FormControl, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';

@Component({
  selector: 'app-pizza-form',
  templateUrl: './pizza-form.component.html',
  styleUrls: ['./pizza-form.component.scss']
})
export class PizzaFormComponent implements OnInit, OnChanges {
  exists: boolean = false;
  @Input() pizza: Pizza;
  @Input() toppings: Topping[];
  @Output() selected: EventEmitter<Topping[]>
  @Output() create: EventEmitter<Pizza>
  @Output() delete: EventEmitter<Pizza>
  @Output() pizzaChange: EventEmitter<Pizza>

  form = this.fb.group({
    id: '',
    name: ['', Validators.required],
    toppings: [[]]
  });
  constructor(private fb: FormBuilder) {
    this.selected = new EventEmitter()
    this.create = new EventEmitter()
    this.delete = new EventEmitter()
    this.pizzaChange = new EventEmitter()
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.pizza && this.pizza.id) {
      this.exists = true
      this.form.patchValue(this.pizza);
    }
    this.form.get('toppings').valueChanges.subscribe((value: Topping[]) => this.selected.emit(value))
  }
  get nameControl() {
    return this.form.get('name') as FormControl;
  }
  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched;
  }
  onCreate() {
    const {value, valid} = this. form
    if (valid) {
      this.create.emit(value)
    }
  }
  onChange() {
    const {value, valid, touched} = this. form
    if (touched && valid) {
      this.pizzaChange.emit({...this.pizza, ...value})
  }
}
  onDelete() {
    const {value, valid} = this. form
    console.log('value-->', value)
    this.delete.emit(value)
  }
}

