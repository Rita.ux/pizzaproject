import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Topping} from '../models/topping.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToppingsService {
  constructor (private http: HttpClient) {}
   getToppings(): Observable<Topping[]> {
     return this.http
       .get<Topping[]>(`http://localhost:3000/toppings`);
    }

}
