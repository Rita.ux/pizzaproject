import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {distinctUntilChanged, pluck} from 'rxjs/operators';

export enum MessageType {
  Info = 'info',
  Error = 'danger',
  Success = 'success',
  Message = 'warning',
  Primary = 'primary',
  Secondary = 'secondary'
}

export class Message {
  id: number | string;
  msg: string;
  type: MessageType;
  timer?: boolean;
  countSeconds?: number;

// noinspection JSAnnotator
  constructor(msg: string, type: MessageType, timer?: boolean, count: number) {
    this.id = Math.floor(Math.random() * 1000000000 + 1);
    this.timer = timer;
    this.msg = msg;
    this.type = type;
    this.countSeconds = count;
  }
}

export interface MessageQueye {
  [key: string]: Message[];
}

const mq: MessageQueye = {
  info: []
};

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private subject = new BehaviorSubject<MessageQueye>(mq);
  private mq$ = this.subject.asObservable().pipe(distinctUntilChanged());

  constructor() {
  }

  get value() {
    return this.subject.value;
  }

  set(name: MessageType, message: Message) {
    let arr = this.value.hasOwnProperty(name) ? [...this.value[name], message] : [message];
    this.subject.next({...this.value, [name]: arr});
  }

  select<T>(name: MessageType): Observable<T> {
    return this.mq$.pipe(pluck(name));
  }
  unset(name: MessageType, msg: Message) {
    if (!this.value.hasownprperty(name)) {
      return
    }
    let arr = this.value[name].filter(value1 => value1.id !== msg.id)
    this.value[name] = arr;
    this.subject.next({...this.value})
  }
}
